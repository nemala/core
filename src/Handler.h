/*
 * Handler.h
 *
 *  Created on: Sep 4, 2014
 *      Author: dave
 */

#ifndef _HANDLER_H_
#define _HANDLER_H_

#include "MessagePropertyTree.h"

namespace NeMALA {

/*
 * Base class for message handling. A derived class must implement the Handle function.
 */
class Handler {
public:
	//-------------- Procedures --------------------

	Handler(){}
	virtual ~Handler(){}

	virtual void Handle(Proptree ptMsgBody)=0;
};

} /* namespace NeMALA */

#endif /* _HANDLER_H_ */
