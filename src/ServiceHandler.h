/*
 * ServiceHandler.h
 *
 *  Created on: Jan 13, 2016
 *      Author: dave
 */

#ifndef _SERVICE_HANDLER_H_
#define _SERVICE_HANDLER_H_

#include "Handler.h"
#include "Publisher.h"
#include <list>
#include <boost/property_tree/ptree.hpp>

namespace NeMALA {

/*
 * Class for handling the service messages.
 * Service message might be timesync from remote server.
 * Pause or resume some nodes at the system (publishers)
 * terminate given node by break the dispatch loop
 */
class ServiceHandler : public Handler
{
public:
	/*
	 * Constructor. Init the class with the given nodeID and add it to the list.
	 * m_bActive is set to true because the node is active.
	 */
	ServiceHandler(unsigned int unNodeId, std::list<Publisher*>* pPublisherList);
	~ServiceHandler(){}

	/*
	 * Scan the entire node list at the system and if a node should receive a service message,
	 * this function will performed this operation by setting the correct values.
	 * @param ptMsgBody: A service message.
	 */
	void Handle(boost::property_tree::ptree ptMsgBody);

	/*
	 * Activate the node.
	 */
	void Activate(){m_bActive = true;}

	/*
	 * Return the status of the m_bActive flag in order to let the dispatcher know about
	 * the state of the node.
	 */
	bool IsActive(){return m_bActive;}

private:
	unsigned int m_unNodeId;
	std::list<Publisher*>* m_pPublisherList;
	bool m_bActive;
};

} /* namespace NeMALA */

#endif /* _SERVICE_HANDLER_H_ */
