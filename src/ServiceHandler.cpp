/*
 * ServiceHandler.cpp
 *
 *  Created on: Jan 13, 2016
 *      Author: dave
 */

#include "ServiceHandler.h"
#include <boost/lexical_cast.hpp>
#include <boost/foreach.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

namespace NeMALA {

ServiceHandler::ServiceHandler(unsigned int unNodeId, std::list<Publisher*>* pPublisherList):
		m_unNodeId(unNodeId),m_pPublisherList(pPublisherList),m_bActive(true){}

void ServiceHandler::Handle(boost::property_tree::ptree ptMsgBody)
{
	// See if they this dispatcher is concerned with the message
	BOOST_FOREACH(boost::property_tree::ptree::value_type &node,ptMsgBody.get_child("nodes"))
	{
		if (!node.second.data().compare("*") || !node.second.data().compare(boost::lexical_cast<std::string>(m_unNodeId)))
		{
			// This message is directed at this dispatcher
			if (0 == ((ptMsgBody.get<std::string>("Operation")).compare("terminate")))
			{
				// If termination is in order, terminate
				m_bActive = false;
				break; // Termination is a good reason to quit the loop. A dispatcher can't die twice...
			}
			else
			{ // Termination is NOT in order
				if (0 == ((ptMsgBody.get<std::string>("Operation")).compare("timesync")))
				{
					// time sync
					boost::posix_time::ptime timeLocal(boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time());
					boost::posix_time::ptime timeGlobal(boost::posix_time::time_from_string(ptMsgBody.get<std::string>("time")));
					BOOST_FOREACH(Publisher* pPublisher, *m_pPublisherList)
					{
						pPublisher->SetTimeDifference(timeGlobal - timeLocal);
					}
				}
				else
				{ // not time sync
					// The operation is either Pause or Resume, and has a "topics" section
					bool bPause(0 == ((ptMsgBody.get<std::string>("Operation")).compare("pause")));
					BOOST_FOREACH(boost::property_tree::ptree::value_type &topic,ptMsgBody.get_child("topics"))
					{
						bool bAllTopics(0 == topic.second.data().compare("*"));
						BOOST_FOREACH(Publisher* pPublisher, *m_pPublisherList)
						{
							// Go over all publishers and check if all topics or the publisher's topic are affected
							if (bAllTopics || (pPublisher->GetTopic() == boost::lexical_cast<int>(topic.second.data())))
							{
								pPublisher->Mute(bPause);
							}
						}
						if (bAllTopics)
						{
							break; // No sense in reading the message for anymore topics. They were all affected just now.
						}
					} // BOOST_FOREACH(boost::property_tree::ptree::value_type &v,ptMsgBody.get_child("topics"))
				} // not time sync
			} // Termination is NOT in order
		} // This message is directed at this dispatcher
	} // BOOST_FOREACH(boost::property_tree::ptree::value_type &node,ptMsgBody.get_child("nodes"))
}

} /* namespace NeMALA */
