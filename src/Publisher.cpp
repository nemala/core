/*
 * Publisher.cpp
 *
 *  Created on: Feb 9, 2015
 *      Author: dave
 */

#include "Publisher.h"
#include "Config.h"
#include <assert.h>
#include <zmq.h>
#include <iostream>
#include <errno.h>
#include <boost/date_time/posix_time/posix_time.hpp>

namespace NeMALA {

//--------------------------------------------------------------------------------

Publisher::Publisher(const char* endpoint, unsigned int topic):m_pPublisher(NULL),
						m_strEndpoint(endpoint), m_bMuted(false), m_unTopic(topic)
{
	if (NEMALA_CORE_SERVICE_TOPIC == m_unTopic)
	{
		throw std::invalid_argument("Publisher constructed with reserved topic number");
	}
}

//--------------------------------------------------------------------------------

Publisher::~Publisher()
{
	if (NULL != m_pPublisher)
	{
		zmq_close(m_pPublisher);
	}
}

//--------------------------------------------------------------------------------

int Publisher::SetContext(void* context)
{
	int result(-1);
	if (NULL == m_pPublisher)
	{
		m_pPublisher = zmq_socket (context, ZMQ_PUB);
		int rc = zmq_connect(m_pPublisher, m_strEndpoint.c_str());
		if (rc != 0)
		{
			std::cout << m_strEndpoint.c_str() << std::endl;
			switch errno
			{
			case EINVAL:
				std::cout << "The endpoint supplied is invalid." << std::endl;
				break;
			case EPROTONOSUPPORT:
				std::cout << "The requested transport protocol is not supported." << std::endl;
				break;
			case ENOCOMPATPROTO:
				std::cout << "The requested transport protocol is not compatible with the socket type." << std::endl;
				break;
			case EADDRINUSE:
				std::cout << "The requested address is already in use." << std::endl;
				break;
			case EADDRNOTAVAIL:
				std::cout << "The requested address was not local." << std::endl;
				break;
			case ENODEV:
				std::cout << "The requested address specifies a nonexistent interface." << std::endl;
				break;
			case ETERM:
				std::cout << "The 0MQ context associated with the specified socket was terminated." << std::endl;
				break;
			case ENOTSOCK:
				std::cout << "The provided socket was invalid." << std::endl;
				break;
			case EMTHREAD:
				std::cout << "No I/O thread is available to accomplish the task." << std::endl;
				break;
			default:
				std::cout << "Unexpected errno:" << errno << std::endl;
			}
		}
		assert (rc == 0);
		result = 0;
	}
	return result;
}

//--------------------------------------------------------------------------------

void Publisher::Publish(BaseMessage& msg)
{
	if (!m_bMuted)
	{
		// Time correction
		boost::posix_time::ptime rawTime(boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time());
		rawTime += m_tdSyncDelta;
		std::string timeStamp(boost::posix_time::to_simple_string(rawTime));
		// Set the time at the header
		msg.SetHeader(timeStamp, m_unTopic);
		std::string strMsg(msg.GetAsString());
		//std::cout << strMsg << std::endl;
		zmq_send(m_pPublisher,strMsg.c_str(), strMsg.size(), 0);
	}
}

} /* namespace NeMALA */
