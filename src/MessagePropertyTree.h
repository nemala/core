/*
 * MessagePropertyTree.h
 *
 *  Created on: Jan 21, 2016
 *      Author: dave
 */

#ifndef _MESSAGE_PROPERTY_TREE_H_
#define _MESSAGE_PROPERTY_TREE_H_

#include "BaseMessage.h"
#include <boost/property_tree/ptree.hpp>

namespace NeMALA {

typedef boost::property_tree::ptree Proptree;

/*
 * Abstract class which builds a message as a property tree.
 * As a BaseMessage it implements the GetAsString method.
 */
class MessagePropertyTree: public BaseMessage
{
public:

	//-------------- Procedures --------------------
	/*
	 * An empty property tree which include the root, header with topic and time,
	 * and a body.
	 */
	MessagePropertyTree();
	virtual ~MessagePropertyTree(){}

	/*
	 * Update the header using these functions.
	 * @param topic: The topic of the message.
	 * @param time: the time stamp of the message (sent time).
	 */
	void SetTopic(unsigned int topic);
	void SetTime(std::string time);

	/*
	 * Returns an XML string representation of the message classes.
	 */
	std::string GetAsString();

	/*
	 * Returns a JSON string representation of the message classes.
	 * Note: this function does not support typed elements, and can be used only
	 * in derived classes which populate their elements using only AddElement in
	 * the BuildMessage function.
	 */
	std::string GetAsStringJson();

protected:

	/*
	 * This function is here to encourage the user to build the message using the AddElement functions.
	 * A variety of messages can be defined using this function.
	 */
	virtual void BuildMessage()=0;

	/*
	 * Reset the message, this is useful when aggregating other messages, so that only one element per
	 * attribute is used. It is protected so that this feature is only used by derived message classes
	 * which hopefully will use it with caution.
	 */
	void ResetMessage();

	/*
	 * Adds an untyped element named strElementName with value strElementValue to the message
	 */
	void AddElement(std::string strElementName, std::string strElementValue);

	/*
	 * Adds a text element named strElementName with value strElementValue to the message
	 */
	void AddElementText(std::string strElementName, std::string strElementValue);

	/*
	 * Adds a number element named strElementName with value strElementValue to the message
	 */
	void AddElementNumber(std::string strElementName, double dElementValue);

private:
	//-------------- Variables --------------------
	Proptree m_pt;

};

} /* namespace NeMALA */

#endif /* _MESSAGE_PROPERTY_TREE_H_ */
