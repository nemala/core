#ifndef _BASEMESAGE_H_
#define _BASEMESAGE_H_

#include <string>

namespace NeMALA {
/*
 * Abstract class which create the header of messages.
 * Each type of messages at the system will derived from it and implement GetAsString
 */
class BaseMessage
{
public:
	//-------------- Procedures --------------------
	BaseMessage():m_unTopic(0){}
	virtual ~BaseMessage(){}

	//Set the data at the header. The correct Topic and Time
	virtual void SetHeader(std::string time, unsigned int topic) {SetTopic(topic); SetTime(time);}

	//Setters and Getters
	virtual void SetTopic(unsigned int topic) {m_unTopic = topic;}
	virtual void SetTime(std::string time) {m_tTime = time;}
	unsigned int GetTopic() {return m_unTopic;}
	std::string GetTime() {return m_tTime;}

	// Returns a string representation of derived message classes
	virtual std::string GetAsString()=0;

private:
	//-------------- Variables --------------------
	unsigned int m_unTopic;
	std::string m_tTime;
};

} /* namespace NeMALA */

#endif /* _BASEMESAGE_H_ */
