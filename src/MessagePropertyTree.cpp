/*
` * MessagePropertyTree.cpp
 *
 *  Created on: Jan 21, 2016
 *      Author: dave
 */

#include "MessagePropertyTree.h"
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace NeMALA {

//--------------------------------------------------------------------------------

MessagePropertyTree::MessagePropertyTree()
{
	ResetMessage();
}

//--------------------------------------------------------------------------------

void MessagePropertyTree::SetTopic(unsigned int topic)
{
	BaseMessage::SetTopic(topic);
	m_pt.get_child("Message.Header.Topic").put("", GetTopic());
}

//--------------------------------------------------------------------------------

void MessagePropertyTree::SetTime(std::string time)
{
	BaseMessage::SetTime(time);
	m_pt.get_child("Message.Header.TimeStamp").put("", GetTime());
}

//--------------------------------------------------------------------------------

void MessagePropertyTree::ResetMessage()
{
	Proptree topic, time, header, body;

	m_pt.erase("Message");

	m_pt.add_child("Message.Header", header);
	m_pt.add_child("Message.Body", body);

	m_pt.add_child("Message.Header.Topic", topic);
	m_pt.add_child("Message.Header.TimeStamp", time);
}

//--------------------------------------------------------------------------------

// Adds an untyped element named strElementName with value strElementValue to the message
void MessagePropertyTree::AddElement(std::string strElementName, std::string strElementValue)
{
	NeMALA::Proptree ptElement;
	ptElement.put("", strElementValue);
	m_pt.get_child("Message.Body").add_child(strElementName, ptElement);
}

//--------------------------------------------------------------------------------

// Adds a text element named strElementName with value strElementValue to the message
void MessagePropertyTree::AddElementText(std::string strElementName, std::string strElementValue)
{
	Proptree ptElement;
	// Attribute text
	ptElement.put("<xmlattr>.type", "text");
	ptElement.put("", strElementValue);

	m_pt.get_child("Message.Body").add_child(strElementName, ptElement);
}

//--------------------------------------------------------------------------------

// Adds a number element named strElementName with value strElementValue to the message
void MessagePropertyTree::AddElementNumber(std::string strElementName, double dElementValue)
{
	Proptree ptElement;
	// Attribute real
	ptElement.put("<xmlattr>.type", "real");
	ptElement.put("", dElementValue);

	m_pt.get_child("Message.Body").add_child(strElementName, ptElement);
}

//--------------------------------------------------------------------------------

// Returns a string representation of the message classes. A conversion from xml to string is done here.
std::string MessagePropertyTree::GetAsString()
{
	std::ostringstream os;

	BuildMessage();
	boost::property_tree::xml_parser::write_xml(os, m_pt);
	return os.str();
}

//--------------------------------------------------------------------------------

//Returns a JSON string representation of the message classes.
std::string MessagePropertyTree::GetAsStringJson()
{
	std::ostringstream os;

	BuildMessage();
	boost::property_tree::json_parser::write_json(os, m_pt, false);
	return os.str();
}

} /* namespace NeMALA */
