/*
 * Dispatcher.h
 *
 *  Created on: Feb 8, 2015
 *      Author: dave
 */

#ifndef _DISPATCHER_H_
#define _DISPATCHER_H_

#include "Handler.h"
#include "ServiceHandler.h"
#include "Publisher.h"
#include <map>
#include <set>

namespace NeMALA {
/*
 * Class for dispatch messages. Every publisher or subscriber has a dispatcher in order
 * to handle them.
 */
class Dispatcher
{
public:

	//-------------- Procedures --------------------

	/*
	 * Constructor. Init the class. It also add to the map the handler of service topic.
	 * @param nVersionMajor: Major version of the dispatcher.
	 * @param nVersionMinor: Minor version of the dispatcher.
	 * @param unNodeID: ID of the node which created the dispatcher.
	 * @param sProxyBackEnd: The back end port of the service proxy. Make sure that each dispatcher
	 * will obey to service messages.
	 */
	Dispatcher(int nVersionMajor, int nVersionMinor, unsigned int unNodeId, std::string sProxyBackEnd);
	virtual ~Dispatcher();

	// Prints the version number to standard output.
	void PrintVersion();

	/*
	 * Links a topic to a handler by mapping the topic to the handler.
	 * @param unTopic: The topic which be the key.
	 * @param pHandler: Pointer to the handler function.
	 * @throw invalid_argument in a case of topic is the reserve topic for service handler.
	 */
	void AddHandler(unsigned int unTopic, Handler* pHandler);

	/*
	 *  Links a publisher with the dispatcher context, and add it to the publishers list.
	 *  @param pPublusher: Pointer to the publisher.
	 */
	void AddPublisher(Publisher* pPublisher);

	/*
	 * Subscribe to endpoint.
	 * @param endpoint: The end point of a proxy (In order to recieve messages).
	 * @throw invalid_argument in a case of endpoint is the endpoint of the service topic.
	 * The subscription to the service topic is done automatically at the constructor.
	 */
	void Subscribe(const char* endpoint);

	/*
	 * Waits for incoming messages, then dispatches the messages and waits again.
	 * As long as the node is active the loop continue and each message read as property tree
	 * and handled by it's handler function.
	 */
	void Dispatch();

	
private:

	//---------------------------------------------

	typedef std::set<Handler*> HandlerSet;

	//-------------- Variables --------------------
	int m_nVersionMajor;
	int m_nVersionMinor;
	std::list<Publisher*> m_publisherList;
	std::map<unsigned int,HandlerSet> m_handlerSetMap;
	std::set<std::string> m_setSubscribedBackends;
	void* m_pContext;
	void* m_pSubscriber;
	ServiceHandler m_ServiceHandler;
};

} /* namespace NeMALA */


#endif /* _DISPATCHER_H_ */
