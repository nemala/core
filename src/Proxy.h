/*
 * Proxy.h
 *
 *  Created on: Dec 16, 2015
 *      Author: gal
 */

#ifndef PROXY_H_
#define PROXY_H_

namespace NeMALA {
/*
 * Class which generates a proxy. A proxy has 3 ports:  a publisher connection which subscribes to publishers, a subscriber connection which publishes to subscribers and
 * a logger connection which publishes to a logger. Every message received from the publisher socket will be sent to the other ports.
 */
class Proxy
{
public:

	//-------------- Procedures --------------------
	/*
	 * Init all the local pointers to null.
	 */
	Proxy();

	~Proxy(){}

	/*
 	 * Create a context for the proxy, and connect each socket to the right end-point.
 	 * @param publishers: the address the publishers bind to.
 	 * @param subscribers: the address subscribers bind to.
 	 * @param logger: the address of the Log end.
 	 */
	void Init(const char* publishers,const char* subscribers,const char* logger);

	/*
	 * Close all the connection and free the context.
	 */
	void ShutDown();

	/*
	 * Start a proxy with the information set at the init.
	 */
	void operator()();

private:

	//-------------- Variables --------------------
	void* m_pPublisherSocket;
	void* m_pSubscriberSocket;
	void* m_pLoggerSocket;
	void* m_pContext;
};

} /* namespace NeMALA */

#endif /* PROXY_H_ */
