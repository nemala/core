/*
 * Proxy.cpp
 *
 *  Created on: Dec 3, 2015
 *      Author: gal
 */

#include <zmq.h>
#include "Proxy.h"

namespace NeMALA {

Proxy::Proxy():m_pPublisherSocket(NULL),m_pSubscriberSocket(NULL),m_pLoggerSocket(NULL),m_pContext(NULL){}

void Proxy::Init(const char* publishers,const char* subscribers,const char* logger)
{
	// Create context
    m_pContext = zmq_ctx_new();

    // Bind the socket to the correct end point
    m_pPublisherSocket  = zmq_socket(m_pContext, ZMQ_XSUB);
	zmq_bind(m_pPublisherSocket, publishers);

	m_pSubscriberSocket = zmq_socket(m_pContext, ZMQ_XPUB);
	zmq_bind(m_pSubscriberSocket, subscribers);

	m_pLoggerSocket = zmq_socket(m_pContext, ZMQ_PUB);
	zmq_bind(m_pLoggerSocket, logger);
}


void Proxy::ShutDown()
{
	zmq_close(m_pPublisherSocket);
    zmq_close(m_pSubscriberSocket);
    zmq_close(m_pLoggerSocket);
    zmq_ctx_term(m_pContext);

}


void Proxy::operator()()
	{
		zmq_proxy(m_pPublisherSocket, m_pSubscriberSocket, m_pLoggerSocket);
	}

} /* namespace NeMALA */
