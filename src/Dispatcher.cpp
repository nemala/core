/*
 * Dispatcher.cpp
 *
 *  Created on: Feb 8, 2015
 *      Author: dave
 */

#include "Dispatcher.h"
#include "Config.h"
#include <zmq.h>
#include <sstream>
#include <assert.h>
#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>

namespace NeMALA {

//--------------------------------------------------------------------------------

Dispatcher::Dispatcher(int nVersionMajor, int nVersionMinor, unsigned int unNodeId, std::string sProxyBackEnd):
		m_nVersionMajor(nVersionMajor),m_nVersionMinor(nVersionMinor),
		m_ServiceHandler(unNodeId,&m_publisherList)
{
	// Prepare context
    m_pContext = zmq_ctx_new();
    // subscriber
    m_pSubscriber = zmq_socket(m_pContext, ZMQ_SUB);

    int rc = zmq_setsockopt(m_pSubscriber, ZMQ_SUBSCRIBE,"", 0);
    assert (rc == 0);

    // Initialize the set of subscribed to end points
    m_setSubscribedBackends.insert(sProxyBackEnd);

    // Connect to Socket
    rc = zmq_connect(m_pSubscriber,sProxyBackEnd.c_str());
    assert (rc == 0);

    // Add the service handler to the handler-set map
    m_handlerSetMap[NEMALA_CORE_SERVICE_TOPIC].insert(&m_ServiceHandler);
}

//--------------------------------------------------------------------------------

Dispatcher::~Dispatcher()
{
	zmq_close(m_pSubscriber);
	zmq_ctx_term(m_pContext);
}

//--------------------------------------------------------------------------------

void Dispatcher::PrintVersion()
{
	std::cout << "Application Version " << m_nVersionMajor	<< "." << m_nVersionMinor << std::endl;
	// Dispatcher Version
	std::cout << "NeMALA Version " << NEMALA_CORE_VERSION_MAJOR << "." << NEMALA_CORE_VERSION_MINOR << std::endl;
	// ZMQ version
	int major, minor, patch;
	zmq_version (&major, &minor, &patch);
	std::cout << "ZMQ version " << major << "." <<  minor << "." << patch << std::endl;
}

//--------------------------------------------------------------------------------

void Dispatcher::AddHandler(unsigned int unTopic, Handler* pHandler)
{
	if (NEMALA_CORE_SERVICE_TOPIC == unTopic)
	{
		throw std::invalid_argument("Dispatcher::AddHandler: added a handler with a reserved topic number");
	}
	m_handlerSetMap[unTopic].insert(pHandler);
}

//--------------------------------------------------------------------------------

void Dispatcher::AddPublisher(Publisher* pPublisher)
{
	int rc = pPublisher->SetContext(m_pContext);
	m_publisherList.insert(m_publisherList.end(), pPublisher);
	assert(0 == rc);
}

//--------------------------------------------------------------------------------

void Dispatcher::Subscribe(const char* endpoint)
{
	// ignore requests to subscribe twice to an end-point.
	if(m_setSubscribedBackends.find(endpoint) == m_setSubscribedBackends.end())
	{
		m_setSubscribedBackends.insert(endpoint);
		int rc = zmq_connect(m_pSubscriber,endpoint);
		assert (rc == 0);
	}
}

//--------------------------------------------------------------------------------

void Dispatcher::Dispatch()
{
	zmq_msg_t msg;
	int rc = zmq_msg_init(&msg);
	assert (rc == 0);
	m_ServiceHandler.Activate();

    while (m_ServiceHandler.IsActive())
    {
    	rc = zmq_msg_recv(&msg, m_pSubscriber, 0);
    	if (-1 == rc)
    	{
    		std::cerr << "ZMQ Receive failed" << std::endl;
    	}
    	else
    	{	
    		bool bInterested(true);
    		unsigned int unTopic(0);
    		char* data = (char*)zmq_msg_data(&msg);
    		std::string str(data,zmq_msg_size(&msg));
    		std::istringstream incomingMsg;
    		incomingMsg.str(str);
    		// std::cout << str;
    		boost::property_tree::ptree ptIncomingMsg;
    		boost::property_tree::read_xml(incomingMsg, ptIncomingMsg);

    		unTopic = ptIncomingMsg.get<unsigned int>("Message.Header.Topic");
    		try
    		{
    			m_handlerSetMap[unTopic];
    		}
    		catch (const std::out_of_range& oor)
    		{
    			// Not interested in this topic.
    			bInterested = false;
    		}
    		if (bInterested)
    		{
    			BOOST_FOREACH(Handler* pHandler, m_handlerSetMap[unTopic])
    			{
    				try
    				{
    					pHandler->Handle(ptIncomingMsg.get_child("Message.Body"));
    				}
    				catch (std::exception& e)
    				{
    					std::cerr << "Error in Dispatcher: " << e.what() << std::endl;
    					zmq_msg_close (&msg);
    					throw e;
    				}
    			}
    		}
    	}
    }
    zmq_msg_close (&msg);
}

//--------------------------------------------------------------------------------
} /* namespace NeMALA */
