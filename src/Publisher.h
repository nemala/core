/*
 * Publisher.h
 *
 *  Created on: Feb 9, 2015
 *      Author: dave
 */

#ifndef _PUBLISHER_H_
#define _PUBLISHER_H_

#include <string>
#include "BaseMessage.h"
#include "boost/date_time/posix_time/posix_time_types.hpp"

namespace NeMALA {
/*
 * Class for publishing messages.
 */
class Publisher
{
public:
	//-------------- Procedures --------------------
	/*
	 * @param endpoint: The name of the endpoint to publish to.
	 * @param topic: The topic of the message.
	 * @throw: invalid_argument in case of service topic is given as parameter.
	 */
	Publisher(const char* endpoint, unsigned int topic);

	/*
	 * Close the context of the publisher
	 */
	virtual ~Publisher();

	/*
	 * Set the context for publishing. If all goes well 0 is returned, otherwise an error will be printed and -1 returned.
	 * @param context: The context of the publisher which will be connected to the
	 * endpoint given at the constructor.
	 */
	int SetContext(void* context);

	/*
	 * Publish a message if not muted, also set the correct time if a timesync operation was
	 * performed.
	 * @param msg: The message which will be sent.
	 */
	void Publish(BaseMessage& msg);

	/*
	 * Mute or unMute the publisher.
	 * @param bMuted: The state of the publisher: True is for mute.
	 */
	void Mute(bool bMuted){m_bMuted = bMuted;}

	/*
	 * Return the topic.
	 */
	unsigned int GetTopic(){return m_unTopic;}

	/*
	 * Set the time difference between this publisher's node and the synchronizing node.
	 */
	void SetTimeDifference(boost::posix_time::time_duration deltaT){m_tdSyncDelta = deltaT;}

private:
	//-------------- Variables --------------------
	void* m_pPublisher;
	unsigned int m_unTopic;
	std::string m_strEndpoint;
	bool m_bMuted;
	boost::posix_time::time_duration m_tdSyncDelta;

};

} /* namespace NeMALA */

#endif /* PUBLISHER_H_ */
