# Get the Docker Image Available at [Docker Hub](https://hub.docker.com/repository/docker/nemala/core)
or
# Install from source
1. ## Install Boost
    1. Follow the instructions at [Boost](http://www.boost.org/doc/libs/1_64_0/more/getting_started/index.html). 
    Recommended steps:
        1. [Download](https://www.boost.org/users/history/version_1_64_0.html) and extract Boost.
        1. From the extracted folder run
            ```console
            user@hostname:/path_to_boost$ ./bootstrap.sh
            ```
        1. If needed, change the generated *project-config.jam* to fit your preferences.
        1. Compile Boost by running
            ```console
            user@hostname:/path_to_boost$ ./b2
            ```
        1. Install Boost by running
            ```console
            user@hostname:/path_to_boost$ sudo ./b2 install
            ```
    1. To cross-compile, which is recommended, follow the instructions [here](https://boostorg.github.io/build/manual/develop/index.html#bbv2.tasks.crosscompile).

1. ## Install ZeroMQ ##
    1. Follow the instructions at [0MQ](http://zeromq.org/intro:get-the-software).
    Recommended steps:
        1. [Download](https://github.com/zeromq/libzmq/releases) latest stable release.
        1. From the extracted folder run
            ```console
            user@hostname:/path_to_zmq$ ./configure; make; make install
            ```
    1. To cross-compile:
        1. From the extracted folder run
            ```console
            user@hostname:/path_to_zmq$ ./configure --host=arm-none-linux-gnueabi CC=/path_to_raspberry_pi_tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf-gcc CXX=/path_to_raspberry_pi_tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf-g++ --prefix=/path_to_raspberry_pi_rootfs/usr/local/ --without-docs
            ```
        1. make and install:
             ```console
             user@hostname:/path_to_zmq$ make; make install
             ```
    
1. ## Install NeMALA Core
    1. Run [CMake](https://cmake.org/)
    1. Set the CMake *source* folder to the NeMALA core *src* folder  and the CMake *build* folder to a separate 
    appropriate folder.
 	1. Run the CMake *Configure* command.
 	1. Select the desired toolchain and makefile generators.
 	1. Run *Configure* again until all variables are filled and no errors appear.
 	    Make sure the install prefix and build type are to your liking.
 	1. Run the CMake *Generate* command.
 	   At this point, the chosen *build* folder has all the make files needed for installing the core library.
    1. Go to the build folder and run the command:
        ```console
        user@hostname:/path_to_build_folder$ make; make install
        ```
