# Components
![Class Diagram](images/Class.svg)
## <a name="MsgBase"></a>Base Message
The Base Message is an abstract class designed to hold the *Topic* and *Time* of any derived message 
class, while enforcing a *GetAsString* function which returns the derived message in a form that may be published.
## <a name="MsgPropTree"></a>Property Tree Message
The Property Tree Message is yet another abstract class, derived from the [Base Message](#MsgBase), implementing the 
Base 
Message's *GetAsString* function by returning an XML string depicting a property tree.

Adding elements to the property tree is facilitated by helper functions, yet the class requires derived classes to implement the <em>BuildMessage</em> function in order to encourage the user to build the message using the <em>AddElement</em> functions.
## <a name="Publisher"></a>Publisher
A Publisher publishes [Base Message](#MsgBase) type messages.
## <a name="Handler"></a>Handler
The Handler abstract class enforces the handling of incoming [Property Tree Messages](#MsgPropTree)  in derived classes.
## <a name="Sandler"></a>Service Handler
Derived from the [Handler](#Handler), the service handler handles service messages, such as:
* Terminate: Notifies the Dispatcher to stop dispatching incoming messages.
* Time-sync: Sets a time difference to synchronize the Dispatcher's timestamps with an external reference clock.
* Pause: Mutes one, some or all of the Dispatcher's Publishers. The Dispatcher's Handlers continue to process incoming
 messages, only the muted Publishers pause their output.
* Resume: Resumes publication of one, some or all muted topics. A Publisher which was paused by a pause command 
previously gets un-muted if its topic is on the resume list.													
## <a name="Dispatcher"></a>Dispatcher
A Dispatcher has a:
* [Handler](#Handler) map, mapping topics to their correct Handlers.
* [Publisher](#Publisher) list, containing various Publishers.
* [Service Handler](#Sandler) to handle incoming service messages.

The Dispatcher is the main process of a [NeMALA](../README.md) logical node.
When implementing a node, the node's main procedure:
1. Constructs a Dispatcher.
1. Constructs [Handlers](#Handler) and [Publishers](#Publisher) as required.
1. Adds the Publishers to the Dispatcher's Publisher list.
1. Adds the Handlers to the Dispatcher's Handler map.
1. Subscribes to various [Proxy](#Proxy) endpoints.
1. Calls the Dispatcher's *Dispatch* function.

The Dispatch function returns when the Dispatcher's built-in [Service Handler](#Sandler) receives a *Terminate* 
service message.
## <a name="Proxy"></a>Proxy
The Proxy provides a *Publishers* endpoint for [Publishers](#Publisher) to publish to,
a *Subscribers* endpoint for [Dispatchers](#Dispatcher) to subscribe to, and a *Logger* endpoint for *Loggers* to 
attach to.

As an example, a network node can run multiple Dispatchers with some topics communicated through a local proxy,
using Inter-Process Communication (ipc), and other topics communicated to other network nodes via another local or 
remote proxy, using Serial or TCP/IP protocols.
