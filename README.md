# NeMALA Core #

A lightweight publisher-subscriber framework.

### Docker Image Available at [Docker Hub](https://hub.docker.com/repository/docker/nemala/core)

## Contents ##
* [Install Guide](doc/start.md)
* [Components](doc/components.md)

## Dependencies ##
| Dependency                        | Minimal Version   | License
| ------------                      | ---------------   | -------
| [Boost](https://www.boost.org/)   | 1.55.0            | [Boost Software License](https://www.boost.org/users/license.html)
| [ZeroMQ](http://zeromq.org/)      | 4.0.4             | [ZeroMQ Free Software License](http://zeromq.org/area:licensing)

## Contributors ##
### [Gal Zamir](http://www.linkedin.com/in/galzamir/) & [Leiran Sobih](http://linkedin.com/in/sobih-leiran-05a912188), Winter 2015-6
* Property Tree Message Class
* Proxy
